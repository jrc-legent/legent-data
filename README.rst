.. figure:: ./_static/logo_legent_data.png
    :align: center
    :alt: alternate text
    :figclass: align-center

.. _start-info:

:versions:      |gh-version| |rel-date|
:documentation: Under construction |doc|
:sources:       https://code.europa.eu/jrc-legent/legent-data
:keywords:      vehicle, real world data.
:short name:    Legent data
:Copyright:     © European Union 2024, CC BY 4.0


.. Caution:: This project is under construction, all documentation here needs to be updated.

.. _end-info:

.. contents:: Table of Contents
    :backlinks: top

.. _start-intro:

Introduction
============

.. _end-intro:

.. _start-intro:
Overview
========
The repository contains a comprehensive collection of datasets produced by LEGENT group
through their respective activities. Available datasets within this repository include the following:

* `JRCMATICS <https://code.europa.eu/jrc-legent/legent-data/-/tree/main/JRCMATICS>`_ - JRC Vehicle teleMATICS

.. _end-overview:

.. _start-download:
Download
========
Download the sources:

.. _end-download:

.. |gh-version| image::  https://img.shields.io/badge/GitHub%20release-dev-orange
    :target: https://github.com/tansial/VDP/tags
    :alt: Latest version in GitHub

.. |rel-date| image:: https://img.shields.io/badge/rel--date-14--07--2022-orange
    :target: https://github.com/JRCSTU/gearshift/releases
    :alt: release date

.. |doc| image:: https://img.shields.io/badge/docs-passing-success
    :alt: GitHub page documentation

.. |CO2| replace:: CO\ :sub:`2`
.. _end-sub:


