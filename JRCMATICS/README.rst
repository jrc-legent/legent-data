.. figure:: ./_static/logo_data.png
    :align: center
    :alt: alternate text
    :figclass: align-center

.. _start-info:

:versions:      |gh-version| |rel-date|
:documentation: |doc|
:sources:       https://code.europa.eu/jrc-legent/legent-data
:keywords:      vehicle, real world data.
:short name:    JRCMATICS data
:Copyright:     © European Union 2024, CC BY 4.0

.. _end-info:

.. contents:: Table of Contents
    :backlinks: top

.. _start-overview:
Overview
========
When using the data presented on this webpage, please cite the following work:

Tansini A., L. Marin A., Suarez J., Aguirre N. and Fontaras G. 
"Learning from the Real-World: Insights on Light-Vehicle Efficiency and CO2 
Emissions from Long-Term On-Board Fuel Consumption Data Collection." 
Available at SSRN: https://ssrn.com/abstract=5060945 or http://dx.doi.org/10.2139/ssrn.5060945 

The data here presented is the result of the JRCMATICS (the JRC-vehicle-teleMATICS) activity, aiming to collect
On-Board Fuel and energy Consumption Monitoring (OBFCM) data and telematics data from road vehicles driven in real-world
conditions. More information provided at the links below:

    * Description: https://green-driving.jrc.ec.europa.eu/JRCmatics_Monitoring_Fuel_Consumption_from_OBD
    * Report: https://green-driving.jrc.ec.europa.eu/JRCMATICS_report

.. _end-overview:

.. _start-download:
Download
========

	|agg-data| **jrcmatics_trips_aggregated_data.csv:** Aggregated data per trip.

	|vehicle-data| **jrcmatics_vehicles_data.csv:** Vehicles specifications.

.. _end-download:

.. _start-documentation:
Documentation
=============

* The data types provided in the jrcmatics_trips_aggregated_data.csv downloadable file include:

:veh_id:                        unique vehicle ID
:trip_start:                    date and time when the trip started [YYYYMMDD-hhmmss]
:distance:                      distance in the trip [km]
:fuel:                          fuel consumed in the trip [l]
:duration:                      trip duration [s]
:mean_speed:                    mean vehicle speed in the trip [km/h]
:std_speed:                     vehicle speed standard deviation in the trip [km/h]
:min_ambient_temp:              min ambient temperature in the trip [°C]
:max_ambient_temp:              max ambient temperature in the trip [°C]
:mean_ambient_temp:             mean ambient temperature in the trip [°C]
:mean_rpm:                      mean engine speed [rpm]
:mean_speed_urban:              mean speed in urban conditions (0 < v < 60) [km/h]
:mean_speed_rural:              mean speed in rural conditions (60 < v < 90) [km/h]
:mean_speed_motorway:           mean speed in motorway conditions (90 < v < 120) [km/h]
:urban_share:                   share of distance driven in urban conditions [%]
:rural_share:                   share of distance driven in rural conditions [%]
:motorway_share:                share of distance driven in motorway conditions [%]
:min_tot_distance:              minimum OBFCM distance in the trip [km]
:max_tot_distance:              maximum OBFCM distance in the trip [km]
:min_tot_fuel:                  minimum OBFCM fuel in the trip [l]
:max_tot_fuel:                  maximum OBFCM fuel in the trip [l]
:avg_fc:                        average fuel consumption in the trip [l/100km]
:avg_co2:                       average |CO2| emissions in the trip [g/km]
:user_type:                     type of user (private, corporate, rental, test)
:fuel_type:                     fuel type (petrol, diesel)
:type:                          powertrain type (ICEV, MHEV, HEV, PHEV)
:science_based_car_segment:     vehicle segment according to https://www.researchsquare.com/article/rs-4378373/v1
:date:                          date when the trip started [DD/MM/YYYY]

* The data types provided in the jrcmatics_vehicles_data.csv downloadable file include:

:veh_id:                        unique vehicle ID
:type:                          powertrain type (ICEV, MHEV, HEV, PHEV)
:fuel_type:                     fuel type (petrol, diesel)
:user_type:                     type of user (private, corporate, rental, test)
:science_based_car_segment:     vehicle segment according to https://www.researchsquare.com/article/rs-4378373/v1
:hybrid_type:                   for hybrid powertrains, the type of hybrid architecture (parallel, serial, powersplit)
:traction:                      traction configuration (FWD, RWD, AWD)
:year_of_registration:          vehicle first registration year
:gearbox:                       type of gearbox (manual, automatic, ...)

.. _end-documentation:

.. _start-contacts:
Contacts
=============

JRC-LDVS-CO2@ec.europa.eu

.. _end-contacts:

.. |gh-version| image::  https://img.shields.io/badge/GitHub%20release-dev-orange
    :target: https://github.com/tansial/VDP/tags
    :alt: Latest version in GitHub

.. |rel-date| image:: https://img.shields.io/badge/rel--date-11--12--2024-orange
    :target: https://github.com/JRCSTU/gearshift/releases
    :alt: release date

.. |doc| image:: https://img.shields.io/badge/docs-passing-success
    :alt: GitHub page documentation

.. |agg-data| image:: https://img.shields.io/badge/Download-20B2AA?
    :target: https://code.europa.eu/jrc-legent/legent-data/-/raw/main/JRCMATICS/jrcmatics_trips_aggregated_data.csv?ref_type=heads&inline=false
    :alt: Download

.. |vehicle-data| image:: https://img.shields.io/badge/Download-20B2AA?
    :target: https://code.europa.eu/jrc-legent/legent-data/-/raw/main/JRCMATICS/jrcmatics_vehicles_data.csv?ref_type=heads&inline=false
    :alt: Download

.. |CO2| replace:: CO\ :sub:`2`
.. _end-sub:


